Rails.application.routes.draw do

  as :user do
    put '/user/confirmation' => 'confirmations#update', :via => :path, :as => :update_user_confirmation
  end

  devise_for :users, controllers: {
      registrations: 'registrations',
      confirmation: 'confirmations'
  }
  devise_for :user, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }
  devise_scope :user do
    delete "/users/sign_out" => "devise/sessions#destroy"
  end
  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #


  root to: "home#index"
end
